



- Create project on gcp
- launch cloud shell
- in cloud shell clone project repository from remote repository (Github, GitLab...)
- inter in directory cloned
- create virtual environnement
- active virtual environment
- install all dependencies
- run app with this command: gunicorn main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000
- create app engine instance:
    - gcloud app create



# Mongodb atlas VPC PEERING 

https://www.youtube.com/watch?v=j6fqXDAdp_o



# Inbound/outbound IP Address

inbound/outbound IP Adress
https://risnotes.medium.com/quickly-setup-inbound-outbound-ip-for-google-app-engine-a00b245fb814

https://producement.com/development/setup-static-ip-for-google-app-engine/

## Setup Inbound IP Address

#### Reserve an external IP address

Reserve an external IP address

<code>gcloud compute addresses create sample-lb-ip --ip-version=IPV4 --global</code>

describe sample-lb-ip ip adress created

<code>gcloud compute addresses describe sample-lb-ip --format="get(address)" --global</code>

#### Create a Network Endpoint Group

<code>gcloud compute network-endpoint-groups create sample-neg --region=asia-east2 --network-endpoint-type=serverless --app-engine-app</code>

#### Create a backend service

<code>gcloud compute backend-services create backend-service --global</code>

#### Add the serverless NEG as a backend to the backend service

<code>gcloud compute backend-services add-backend backend-service --global --network-endpoint-group=sample-neg --network-endpoint-group-region=asia-east2</code>

#### Create a URL map to route incoming requests to the backend service

<code>gcloud compute url-maps create sample-url-map --default-service backend-service</code>

#### Create a Google-managed SSL certificate

<code>gcloud compute ssl-certificates create sample-ssl-cert --domains=demo.momaii.com,api.momaii.com</code>

For HTTPS load balancer, create an HTTPS target proxy. The proxy is the portion of the load balancer that holds the SSL certificate for HTTPS Load Balancing

<code>gcloud compute target-https-proxies create sample-https-proxy --ssl-certificates=sample-ssl-cert --url-map=sample-url-map</code>

#### Create a global forwarding rule to route incoming requests to the HTTPS proxy

<code>gcloud compute forwarding-rules create sample-https-forwarding-rule --address=sample-lb-ip --target-https-proxy=sample-https-proxy --global --ports=443</code>

#### HTTP to HTTPS Redirection

create file called "sample-http-redirect-map.yaml"

and add content bellow:

kind: compute#urlMap
name: sample-http-redirect-map
defaultUrlRedirect:
  redirectResponseCode: MOVED_PERMANENTLY_DEFAULT
  httpsRedirect: True

Create the url maps

<code>gcloud compute url-maps import sample-http-redirect-map --source sample-http-redirect-map.yaml --global</code>

<code>gcloud compute url-maps describe sample-http-redirect-map</code>

Create the http proxy and forwarding rule

<code>gcloud compute target-http-proxies create sample-redirect-lb-proxy --url-map=sample-http-redirect-map --global</code>

<code>gcloud compute forwarding-rules create sample-redirect-forwarding-rule --address=sample-lb-ip --global --target-http-proxy=sample-redirect-lb-proxy --ports=80</code>

All Done. You can now map your DNS with the Load Balancer IP






